/*
 *		interface FloatImg <-> libpnm(3)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include  <pam.h>        

#include "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
static void print_struct_pam(struct pam *ppam, char *txt)
{

printf("    size                %d\n", ppam->size);

printf("    format              %d\n", ppam->format);
printf("    plainformat         %d\n", ppam->plainformat);
printf("    width & height      %d %d\n", ppam->width, ppam->height);
printf("    depth               %d\n", ppam->depth);
printf("    maxval              %lu\n", ppam->maxval);

}
/* --------------------------------------------------------------------- */

int fimg_pnm_infos(char *fname)
{
struct pam		inpam;
FILE			*fp;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, fname);
#endif

if (NULL==(fp=fopen(fname, "r"))) {
	perror(fname);
	exit(1);
	}

pnm_readpaminit(fp, &inpam, sizeof(inpam));

print_struct_pam(&inpam, fname);

fclose(fp);

return 0;
}
/* --------------------------------------------------------------------- */
