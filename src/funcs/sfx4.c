/*
 *		FLOATIMG - a kluge from tTh
 *		---------------------------
 *
 *	some strange effects on floating pictures.
 */


#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>

#include  "../floatimg.h"

extern int		verbosity;

/* -------------------------------------------------------------- */
int fimg_sfx_triplemul(FloatImg *src, FloatImg *dst, int notused)
{
int		x, y, foo;
float		in[3], out[3];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p 0x%04x )\n", __func__, img, notused);
#endif

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		foo =  fimg_get_rgb(src, x, y, in);
		out[0] = in[1] * in[2];
		out[1] = in[0] * in[2];
		out[2] = in[0] * in[1];
		// fprintf(stderr, "%9f %9f %9f\n", out[0], out[1], out[2]);
		foo =  fimg_put_rgb(dst, x, y, out);
		}
	}

return 0;
}
/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */
