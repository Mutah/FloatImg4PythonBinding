#!/bin/bash

#
#	trying to run a maximum of ugly code
#

for trial in $(./t -l)
do

	printf "============ %-10s ============\n" $trial

	make t
	error=$?
	if [ 0 -ne $error ] 
	then
		echo "make error is " $error
		exit 1
	fi


	./t -v $trial
	error=$?
	if [ 0 -ne $error ] 
	then
		echo "run error is " $error
		exit 1
	fi

	printf "\t=== return code %d\n" $error
	echo
	sleep 10

done
