/*
 *		FloatImg from tTh  -  2021
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>

#include  "../floatimg.h"

extern int verbosity;		/* must be declared around main() */

/* --------------------------------------------------------------------- */
void fimg_print_minmax(float minmax[6], char *titre)
{

fprintf(stderr, "\t\tminmax %s\n", titre);
fprintf(stderr, "red\t\t%10f   %10f\n",   minmax[0], minmax[1]);
fprintf(stderr, "green\t\t%10f   %10f\n", minmax[2], minmax[3]);
fprintf(stderr, "blue\t\t%10f   %10f\n",  minmax[4], minmax[5]);
}
/* --------------------------------------------------------------------- */
int parse_WxH(char *str, int *pw, int *ph)
{
// char			*ptr;
int			foo, w, h;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %p %p )\n", __func__,
				str, pw, ph);
#endif

foo = sscanf(str, "%dx%d", &w, &h);
if (2 != foo) {
	fprintf(stderr, "%s : arg '%s' is invalid\n", __func__, str);
	return foo;
	}
*pw = w;	*ph = h;

return 2;
}
/* --------------------------------------------------------------------- */
int parse_double(char *str, double *dptr)
{
double		value;
int		foo;

foo = sscanf(str, "%lf", &value);
if (1 == foo) {
	*dptr = value;
	return 1;
	}
return -1;
}
/* --------------------------------------------------------------------- */
int file_type_from_name(char *name)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, name);
#endif

if (!strcasecmp(name, "pnm" ))	return FILE_TYPE_PNM;
if (!strcasecmp(name, "fimg"))	return FILE_TYPE_FIMG;
if (!strcasecmp(name, "tga" ))	return FILE_TYPE_TGA;
if (!strcasecmp(name, "png" ))	return FILE_TYPE_PNG;
if (!strcasecmp(name, "tiff"))	return FILE_TYPE_TIFF;
if (!strcasecmp(name, "tif" ))	return FILE_TYPE_TIFF;
if (!strcasecmp(name, "fits"))	return FILE_TYPE_FITS;
if (!strcasecmp(name, "exr"))	return FILE_TYPE_EXR;

return -1;
}

/* --------------------------------------------------------------------- */
int print_rectangle(char *str, FimgArea51 *rect)
{
printf("rect @ %p  '%s':\n\t %dx%d  at  %d,%d\n", rect, str,
			rect->w, rect->h, rect->x, rect->y);
return 0;
}
/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */
/*
 *	/!\	return 4 on success
 */
int parse_rectangle(char *str, FimgArea51 *r, int notused)
{
int		x, y, w, h, foo;

if (verbosity)
	fprintf(stderr, "parsing %s\n", str);

foo = sscanf(str, "%d,%d,%d,%d", &w, &h, &x, &y);
if (4 == foo) {
	r->x = x, r->y = y, r->w = w, r->h = h;
	return 4;
	}
return -1;
}

/* --------------------------------------------------------------------- */
int format_from_extension(char *fname)
{
char		*cptr;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, fname);
#endif

cptr = rindex(fname, '.');
if (NULL==cptr) {
	fprintf(stderr, "No dot in %s\n", fname);
	return -1;
	}

#if DEBUG_LEVEL
fprintf(stderr, "\t[%s]  -->  [%s]\n", fname, cptr);
#endif

return file_type_from_name(cptr+1);
}
/* --------------------------------------------------------------------- */
char * extension_from_format(int fmt)
{

switch (fmt) {
	case FILE_TYPE_FIMG:	return ".fimg";		break;
	case FILE_TYPE_PNM:	return ".pnm";		break;
	case FILE_TYPE_PNG:	return ".png";		break;
	case FILE_TYPE_TIFF:	return ".tiff";		break;
	case FILE_TYPE_FITS:	return ".fits";		break;
	case FILE_TYPE_TGA:	return ".tga";		break;

	default:
		fprintf(stderr, "%s: bad %d fmt type\n", __func__, fmt);
		return NULL;
	}

return "???";
}
/* --------------------------------------------------------------------- */
