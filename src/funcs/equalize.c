/*
 *		FLOATIMG
 *	egalisation dynamique approximative
 *	#coronamaison  Thu 09 Apr 2020 03:37:10 PM CEST
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>

#include  "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
int fimg_equalize_compute(FloatImg *src, void *vptr, float vmax)
{
float			minmax[6];
int			foo;
float			dr, dg, db;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %f )\n", __func__, src, vptr, vmax);
#endif

memset(minmax, 0, 6*sizeof(float));

foo = fimg_get_minmax_rgb(src, minmax);
if (foo) {
	fprintf(stderr, "err %d get minmax in %s\n", foo, __func__);
	return foo;
	}

dr = minmax[1] - minmax[0];
dg = minmax[3] - minmax[2];
db = minmax[5] - minmax[4];

printf("Rmin  %12.4g   max  %12.4g  delta  %12.4g\n", minmax[0], minmax[1], dr);
printf("Gmin  %12.4g   max  %12.4g  delta  %12.4g\n", minmax[2], minmax[3], dg);
printf("Bmin  %12.4g   max  %12.4g  delta  %12.4g\n", minmax[4], minmax[5], db);

if ( (minmax[0]<0.0) || (minmax[2]<0.0) || (minmax[4]<0.0) ) {
	fprintf(stderr, "%s: negative value ?\n", __func__);
	return -4;
	}

// printf("deltas  %12.4g  %12.4g  %12.4g\n", dr, dg, db);

return 0;
}
/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */

