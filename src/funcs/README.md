# Fonctions

Plein de fonctions qu'il serait bon de documenter :)

## PNG

__Attention__ : la bibliothèque `pnglite`actuellement utiilsée pour lire les
fichiers PNG n'accepte que **certains** types de fichiers.
Et en particulier, elle brotche sur ceux produits par ImageMagick !

## Contours

Détecter des contours est une activité respectable.

## Exporter

Une méta-fonction qui va sauvegarder (dans la mesure de ses conséquences)
une image en fonction de l'extension du nom de fichier.

## Sfx

Effets spéciaux divers.

## Dithering

Work in progress...
