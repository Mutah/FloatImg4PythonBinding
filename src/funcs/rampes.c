/*
 *		FLOATIMG
 *	rampes diverses, trucs etranges 
 */

#include  <stdio.h>
#include  <stdint.h>

#include  "../floatimg.h"

/* --------------------------------------------------------------------- */
int fimg_hdeg_a(FloatImg *img, double dcoef)
{
int		x, y;
float		value;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %f )\n", __func__, img, dcoef);
#endif

if (FIMG_TYPE_RGB != img->type) {
	fprintf(stderr, "%s bad type\n", __func__);
	return -6;
	}
for (x=0; x<img->width; x++)
	{
	value = (float)x / (float)img->width;
	value *= dcoef;
	for (y=0; y<img->height; y++) {
		fimg_plot_rgb(img, x, y, value, value, value);
		}
	}

return 0;
}
/* --------------------------------------------------------------------- */
/*
 *    To have the black at the bottom, use a negative dcoef
 */
int fimg_vdeg_a(FloatImg *img, double dcoef)
{
int		x, y;
float		value;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %f )\n", __func__, img, dcoef);
#endif

if (FIMG_TYPE_RGB != img->type) {
	fprintf(stderr, "%s bad type\n", __func__);
	return -6;
	}
for (y=0; y<img->height; y++)
	{
	value = (float)y / (float)img->height;
	value *= dcoef;
	for (x=0; x<img->width; x++) {
		fimg_plot_rgb(img, x, y, value, value, value);
		}
	}

return 0;
}
/* --------------------------------------------------------------------- */
