/*
 *		This is an eternal WIP, sorry...
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <math.h>

#include  "../floatimg.h"

/* --------------------------------------------------------------------- */

int fimg_test_pattern(FloatImg *fimg, int type, double dval)
{
int		nio;
int		x, y, k;
float		fr, fg, fb, val;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %g )\n", __func__, fimg, type, dval);
#endif

if (fimg->type != FIMG_TYPE_RGB) {
	fprintf(stderr, "%s need an rgb pic\n", __func__);
	return -6;
	}

/* rampe de primaires dans le quart du haut */
val = (float)dval;
for (x=0; x<fimg->width; x++) {
	nio = x / (fimg->width / 8);
	switch(nio) {
		case 0:	fr = 0.0, fg = 0.0, fb = 0.0;	break;
		case 1:	fr = val, fg = 0.0, fb = 0.0;	break;
		case 2:	fr = 0.0, fg = val, fb = 0.0;	break;
		case 3:	fr = val, fg = val, fb = 0.0;	break;
		case 4:	fr = 0.0, fg = 0.0, fb = val;	break;
		case 5:	fr = val, fg = 0.0, fb = val;	break;
		case 6:	fr = 0.0, fg = val, fb = val;	break;
		case 7:	fr = val, fg = val, fb = val;	break;
		default:
			abort();			break;
		}
	for (y=0; y<fimg->height/4; y++)
			fimg_plot_rgb(fimg, x, y, fr, fg, fb);
	}

k = fimg->height / 4;
for (x=0; x<fimg->width; x++) {
	val =  ((double)x / (double)fimg->width) * dval;
	for (y=0; y<20; y++) {
		fimg_plot_rgb(fimg, x, k+y, val, val, val);
		fimg_plot_rgb(fimg, x, k+y+20, dval-val, dval-val, dval-val);
		}
	// fprintf(stderr, " %6d   %f\n", x, val);
	}

return 0;
}
/* --------------------------------------------------------------------- */
int fimg_draw_something(FloatImg *fimg)
{
int	x, y;
float 	fx, fy;

#define K (3.14159*13.456)
#define M 100.0

for (x=0; x<fimg->width; x++) {
	fimg_plot_rgb(fimg, x, 0,
		(float)x,
		(float)x + 5.678,
		(float)x * 1.33333);
	}

for (y=1; y<fimg->height; y++) {
	fy = (float)y / (float)fimg->height;
	for (x=0; x<fimg->width; x++) {
		fx = (float)x / (float)fimg->width;
		fimg_plot_rgb(fimg, x, y,
			M*(cos(fx*K)+1.2),
			M*(sin(fy*K)+1.4),
			M*(cos(fx*fy)+1.6));
		}
	}
return 0;
}
/* --------------------------------------------------------------------- */
int fimg_mircol_1(FloatImg *dst, float mval)
{
int		x, y;
float		fx, fy, rgb[3];

for (y=0; y<dst->height; y++) {
	fy = (float)y / (float)dst->height;
	for (x=0; x<dst->width; x++) {
		fx = (float)x / (float)dst->width;
		rgb[0] = mval * fx;
		rgb[1] = mval * ((fx+fy)/2.0);
		rgb[2] = mval * fy;
		fimg_put_rgb(dst, x, y, rgb);
		}
	}
return -1;
}
/* --------------------------------------------------------------------- */
int fimg_multirandom(FloatImg *fimg, long nbpass)
{
int	foo, x, y;

#define RI ( (rand()/7) + (rand()/9) )
#define RD ( (drand48()/7) + (drand48()/7) )

for (foo=0; foo<nbpass; foo++)
	{
	x = RI % fimg->width;
	y = RI % fimg->height;
	fimg_add_rgb(fimg, x, y, RD, RD, RD);
	}
return 0;
}
/* --------------------------------------------------------------------- */
