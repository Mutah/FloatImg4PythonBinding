/*
 *		FLOATIMG
 *	rotation matricielle des images
 *	#coronamaison  Mon 23 Mar 2020 11:45:59 AM CET
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>

#include  "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
int fimg_rotate_90(FloatImg *src, FloatImg *dst, int notused)
{
int			foo;
int			x, y, k;
float			rgb[3];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__,
				src, dst, notused);
#endif

if (src->type != FIMG_TYPE_RGB) {
	fprintf(stderr, "%s: src type %d not valid\n", __func__,
					src->type);
	return -6;
	}

/* check if dst pic is not allocated */
if ( 0 == (dst->type | dst->width | dst->height) ) {
#if DEBUG_LEVEL
	fprintf(stderr, "in %s, %p is empty\n", __func__, dst);
#endif
	/* OK allocate a new fpic */
	foo = fimg_create(dst, src->height, src->width, src->type);
	if (foo) {
		fprintf(stderr, "%s: err %d create new pic\n", __func__, foo);
		return -887;
		}
	// if (verbosity>1) fimg_describe(dst, "new pic");
	}

/* check if dst and src are conpatibles */
if ( (src->type != dst->type)    ||
     (src->width != dst->height) || (src->height != dst->width) ) {
	fprintf(stderr, "%s: src & dst not compatibles\n", __func__);
	return -888;
	}

/*
 *		THIS IS A CRUDE IMPLEMENTATION
*/
for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		fimg_get_rgb(src, x, y, rgb);
		// XXX ??? j = (dst->height - x) - 1;
		k = (dst->width  - y) - 1;
#if DEBUG_LEVEL > 1
		fprintf(stderr, "%6d %6d\n", k, j);
#endif
		fimg_put_rgb(dst, k, x, rgb);
		}
	}

/* we don't have any cleanup to make */

return 0;
}
/* --------------------------------------------------------------------- */
