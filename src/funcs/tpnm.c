/*
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include  <pam.h>

#include "../floatimg.h"

int fimg_pnm_infos(char *);

/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo;
char		*infile = "foo.pnm";

pnm_init(&argc, argv);

if (2 == argc)	infile = argv[1];

foo = fimg_pnm_infos(infile);
fprintf(stderr, "got %d\n", foo);

return 0;
}
/* --------------------------------------------------------------------- */
