/*
                RECURSION 'QUADTREE' SUR LES IMAGES
                -----------------------------------
*/

#include   <stdio.h>
#include   <math.h>
#include  <stdint.h>

#include   "../floatimg.h"

/* -------------------------------------------------------------------- */
/*		may be we need some private variables ? 		*/
/* --------------------------------------------------------------------	*/
/* nouveau 29 avril 2021, pendant un autre masque-flamme coronavidique	*/

int fimg_recursion_proto(FloatImg *src, FloatImg *dst, int notused)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__, src, dst, notused);
#endif

fprintf(stderr, "!!!!!! %s is a wip !!!!!\n", __func__);

return -1;
}
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
