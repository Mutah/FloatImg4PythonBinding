/*
 *		FLOATIMG
 *	effets spéciaux àlc sur les couleurs 
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>

#include  "../floatimg.h"

/* --------------------------------------------------------------------- */
/*
 *	OMG ! a Color Graphic Adaptor emulator :)
 */
int fimg_killcolors_a(FloatImg *fimg, float fval)
{
int		nbpix, foo;

if (FIMG_TYPE_RGB != fimg->type) {
	fprintf(stderr, "%s: bad src type %d on %p\n", __func__,
					fimg->type, fimg);
	return -8;
	}

nbpix = fimg->width * fimg->height;
for (foo=0; foo<nbpix; foo++) {
	if (fimg->R[foo] > fimg->G[foo])
		fimg->B[foo] = fimg->R[foo];
	else
		fimg->B[foo] = fimg->G[foo];
		}

return 0;
}
/* --------------------------------------------------------------------- */
/*
 *	parameter fval is not used, why ?
 */
int fimg_killcolors_b(FloatImg *fimg, float fval)
{
int		nbpix, foo;

if (FIMG_TYPE_RGB != fimg->type) {
	fprintf(stderr, "%s: bad src type %d on %p\n", __func__,
					fimg->type, fimg);
	return -8;
	}

nbpix = fimg->width * fimg->height;
for (foo=0; foo<nbpix; foo++) {
	if (fimg->R[foo] > fimg->B[foo])
		fimg->G[foo] = fimg->R[foo];
	else
		fimg->G[foo] = fimg->B[foo];
		}

return 0;
}
/* --------------------------------------------------------------------- */
int fimg_colors_mixer_a(FloatImg *fimg, float fval)
{
int		nbpix, foo;
float		R, G, B;

if (FIMG_TYPE_RGB != fimg->type) {
	fprintf(stderr, "%s: bad src type %d on %p\n", __func__,
					fimg->type, fimg);
#if MUST_ABORT
	abort();
#endif
	return -8;
	}

nbpix = fimg->width * fimg->height;

for (foo=0; foo<nbpix; foo++) {

	R = (fimg->G[foo] + fimg->B[foo]) / fval;
	G = (fimg->R[foo] + fimg->B[foo]) / fval;
	B = (fimg->R[foo] + fimg->G[foo]) / fval;

	fimg->R[foo] = R;
	fimg->G[foo] = G;
	fimg->B[foo] = B;
	}

return 0;
}
/* --------------------------------------------------------------------- */
