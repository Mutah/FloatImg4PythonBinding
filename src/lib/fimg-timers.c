/*
 *	timers.c		BUGS INSIDE ?
 */

#include  <stdio.h>
#include  <sys/time.h>

extern int		verbosity;

/* ----------------------------------------------------------------- */
static double dtime(void)
{
struct timeval	t;
double		d;
(void)gettimeofday(&t, NULL);
d = (double)t.tv_sec + (double)t.tv_usec / 1e6;
return d;
}
/* ----------------------------------------------------------------- */
/* we can have only one timer at this time. patches welcome.*/
static double	memory_time;

double fimg_timer_set(int whot)
{
double		current;

current = dtime();

#if DEBUG_LEVEL
fprintf(stderr, "%s ( %d ) -> current %f\n", __func__, whot, current);
#endif
memory_time = current;
return memory_time;
}
/* ----------------------------------------------------------------- */
double fimg_timer_get(int whot)
{
double		current;
current = dtime();
return current - memory_time;
}
/* ----------------------------------------------------------------- */
