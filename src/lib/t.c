/*
 *		programme de test pour
 *		les fonctions de base.
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <string.h>
#include  <math.h>

#include  "../floatimg.h"

int		verbosity;

/* ---------------------------------------------------------------- */
int essai_timer(int uuuh)
{
double		A, B;

fprintf(stderr, ">>> %s ( %d )\n", __func__, uuuh);

A = fimg_timer_set(uuuh);
sleep(4);
B = fimg_timer_get(uuuh);

fprintf(stderr, "   %f      %f\n", A, B);

return 0;
}
/* ---------------------------------------------------------------- */
/* ---------------------------------------------------------------- */
#define  WI	1024
#define  HI	768
#define  KDEG	3.141592654		/* secret magic number */

int essai_interpolate(int k)
{
FloatImg	A, B, C;
int		foo, idx;
char		ligne[200];
float		fval, minmax[6];

foo = fimg_create(&A, WI, HI, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "%s err create A %d\n", __func__, foo);
	return foo;
	}
fimg_hdeg_a(&A, KDEG);

foo = fimg_create(&B, WI, HI, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "%s err create B %d\n", __func__, foo);
	return foo;
	}
fimg_vdeg_a(&B, KDEG);

foo = fimg_create(&C, WI, HI, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "%s err create C %d\n", __func__, foo);
	return foo;
	}

#define NB	16
for (idx=0; idx<NB; idx++) {
	fval = (float)idx / (float)NB;
	if (verbosity) fprintf(stderr, "%4d  %f\n", idx, fval);
	foo = fimg_interpolate(&A, &B, &C, fval);
	if (foo) {
		fprintf(stderr, "%s err interpolate %d\n", __func__, foo);
		return foo;
		}
	sprintf(ligne, "polate-%02d.pnm", idx);
	foo = fimg_save_as_pnm(&C, ligne, 0);
	}
/*
 $ convert -delay 10 polate-* foo.gif ; animate foo.gif
 */
fimg_destroy(&A);	fimg_destroy(&B);	fimg_destroy(&C);

return 0;
}
/* ---------------------------------------------------------------- */

int essai_2gray(FloatImg *picz, char *outname)
{
int		foo;
FloatImg	gray;

fprintf(stderr, ">>> %s ( %p '%s' )\n", __func__, picz, outname);

foo = fimg_create(&gray, picz->width, picz->height, FIMG_TYPE_GRAY);
if (foo) {
	fprintf(stderr, "%s : err %d on fimg create\n", __func__, foo);
	exit(1);
	}
foo = fimg_mk_gray_from(picz, &gray, 0);
if (foo) {
	fprintf(stderr, "%s : err %d on fimg mk_gray_from\n", __func__, foo);
	exit(1);
	}

foo = fimg_save_as_pnm(&gray, outname, 0);
if (foo) {
	fprintf(stderr, "%s : err %d on save_as_pnm\n", __func__, foo);
	exit(1);
	}

fimg_destroy(&gray);

return 0;
}
/* ---------------------------------------------------------------- */
#define TAILLE		1024

int essai_clone_et_copy(int unused)
{
FloatImg	A, B, C;
int		foo;

fprintf(stderr, "-------- %s ( %d ) --------\n", __func__, unused);

foo = fimg_create(&A, TAILLE, TAILLE, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "%s err create A %d\n", __func__, foo);
	return foo;
	}
foo = fimg_draw_something(&A);
if (foo) {
	fprintf(stderr, "%s err drawing A %d\n", __func__, foo);
	return foo;
	}

foo = fimg_save_as_pnm(&A, "A.pnm", 0);
if (foo) {
	fprintf(stderr, "%s : err %d on save_as_pnm\n", __func__, foo);
	exit(1);
	}

foo = fimg_clone(&A, &B, 1);
if (foo) {
	fprintf(stderr, "%s err clone B %d\n", __func__, foo);
	return foo;
	}

foo = fimg_create(&C, TAILLE, TAILLE, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "%s err create A %d\n", __func__, foo);
	return foo;
	}
foo = fimg_copy_data(&A, &C);
if (foo) {
	fprintf(stderr, "%s err copydata %d\n", __func__, foo);
	return foo;
	}
foo = fimg_save_as_pnm(&C, "C.pnm", 0);

fimg_destroy(&A); fimg_destroy(&B); fimg_destroy(&C);

return 0;
}

#undef TAILLE

/* ---------------------------------------------------------------- */
int essai_get_values(char *fname)
{
int		foo;
FloatImg 	dessin;
float		vals[6];

foo = fimg_create_from_dump(fname, &dessin);
if (foo) {
	fprintf(stderr, "in %s, error %d loading '%s'\n",
			__func__,	foo, fname);
	return foo;
	}

foo = fimg_get_minmax_rgb(&dessin, vals);
if (foo) {
	fprintf(stderr, "%s: err %d on fimg_get_minmax_rgb\n",
			__func__, foo);
	return foo;
	}

for (foo=0; foo<6; foo++) {
	fprintf(stderr, "%7d    %17.6g\n", foo, vals[foo]);
	}

return -1;
}
/* ---------------------------------------------------------------- */

int essai_contraste(char *fname)
{
int		foo;
FloatImg 	dessin, copy;
double		maxi;

fprintf(stderr, "-------- %s ( '%s' ) --------\n", __func__, fname);

foo = fimg_create_from_dump(fname, &dessin);
if (foo) {
	fprintf(stderr, "in %s, error %d loading '%s'\n",
			__func__,	foo, fname);
	return foo;
	}

foo = fimg_clone(&dessin, &copy, 0);
fimg_save_as_pnm(&dessin, "dessin.pnm", 0);

maxi = (double)fimg_get_maxvalue(&dessin);
fprintf(stderr, "image source        valeur maxi = %f\n", maxi);

fimg_power_2(&dessin, &copy, maxi);
maxi = (double)fimg_get_maxvalue(&copy);
fprintf(stderr, "apres  power_2      valeur maxi = %f\n", maxi);
fimg_save_as_pnm(&copy, "power2.pnm", 0);

fimg_square_root(&dessin, &copy, maxi);
maxi = (double)fimg_get_maxvalue(&copy);
fprintf(stderr, "apres  square_root  valeur maxi = %f\n", maxi);
fimg_save_as_pnm(&copy, "squareroot.pnm", 0);

fimg_cos_01(&dessin, &copy, maxi);
maxi = (double)fimg_get_maxvalue(&copy);
fprintf(stderr, "apres  cos 01       valeur maxi = %f\n", maxi);
fimg_save_as_pnm(&copy, "cos_01.pnm", 0);

fimg_cos_010(&dessin, &copy, maxi);
maxi = (double)fimg_get_maxvalue(&copy);
fprintf(stderr, "apres  cos 010      valeur maxi = %f\n", maxi);
fimg_save_as_pnm(&copy, "cos_010.pnm", 0);

fimg_destroy(&dessin);		fimg_destroy(&copy);

return 0;
}
/* ---------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt;
// char		outname[100];
int		gray = 0;

while ((opt = getopt(argc, argv, "gn:v")) != -1) {
	switch(opt) {
		case 'g':	gray++;			break;
		case 'n':	foo=atoi(optarg);	break;
		case 'v':	verbosity++;		break;
		default:
			fprintf(stderr, "%s: oh, %c is a bad opt.\n",
						argv[0], opt);
			exit(5);
		}
	}

if (verbosity)	{
	fimg_print_version(1);
	fimg_print_sizeof();
	}

foo = essai_contraste("quux.fimg");
fprintf(stderr, "retour essai contraste -> %d\n", foo);

// foo = essai_clone_et_copy(0);
// fprintf(stderr, "retour essai clone'n'copy -> %d\n", foo);

// foo = essai_timer(0);
// fprintf(stderr, "retour essai timer -> %d\n", foo);

return 0;
}
