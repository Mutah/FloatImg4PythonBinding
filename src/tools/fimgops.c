/*
		FIMGOPS
*/
#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>
#include  <unistd.h>

#include  "../floatimg.h"

int		verbosity;

/* --------------------------------------------------------------------- */

float		global_fvalue;

/* --------------------------------------------------------------------- */
#define		OP_ADD		1
#define		OP_SUB		2
#define		OP_MIX		3
#define		OP_MUL		4
#define		OP_MINI		5
#define		OP_MAXI		6
typedef struct {
	int	code;
	char	*op;
	} Opcode;

Opcode opcodes[] = {
	{ OP_ADD,		"add"		},
	{ OP_SUB,		"sub"		},
	{ OP_MIX,		"mix"		},
	{ OP_MUL,		"mul"		},
	{ OP_MINI,		"mini"		},
	{ OP_MAXI,		"maxi"		},
	{ 0,			NULL		}
	};
static void pr_opcodes(void)
{
Opcode *optr;
puts("operators:");
for (optr = opcodes; optr->code; optr++) {
	printf("\t%-15s %d\n", optr->op, optr->code); 
	} 
}
static int look_opcode(char *txt)
{
Opcode *optr;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, txt);
#endif

for (optr = opcodes; optr->code; optr++) {
	if (!strcmp(txt, optr->op)) {
		// printf("found %s as %d\n", optr->op, optr->code);
		return optr->code;
		}
	} 
return -1;
}
/* --------------------------------------------------------------------- */
static void help(int lj)
{

puts("usage:\n\tfimgops [options] A.fimg B.fimg operator D.fimg");
puts("options:");
// puts("\t-g              convert output to gray");
printf("\t-k N.N\t\tset float value (def=%.3f)\n", global_fvalue);
puts("\t-v\t\tincrease verbosity");
pr_opcodes();
if (verbosity) fimg_print_version(1);
exit(0);
}
/* --------------------------------------------------------------------- */
int exec_operator(FloatImg *A, FloatImg *B, int action, FloatImg *D)
{
int	foo;

switch (action)	{

	case OP_ADD:
		foo = fimg_add_3(A, B, D);		break;
	case OP_SUB:
		foo = fimg_sub_3(A, B, D);		break;
	case OP_MIX:
		if (verbosity) fprintf(stderr, "%s:mix: fvalue is %f\n",
						__func__, global_fvalue);
		foo = fimg_interpolate(A, B, D, global_fvalue);
		break;
	case OP_MUL:
		foo = fimg_mul_3(A, B, D);		break;
	case OP_MINI:
		foo = fimg_maximum(A, B, D);		break;
	case OP_MAXI:
		foo = fimg_minimum(A, B, D);		break;
	default:
		fprintf(stderr, "fscking action #%d\n", action);
		foo = -99;				break;

	}

return foo;
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt, action;
char		*operator;
FloatImg	srcA, srcB, dest;

global_fvalue = 0.5;

while ((opt = getopt(argc, argv, "hk:v")) != -1) {
	switch(opt) {
		case 'g':					break;
		case 'h':	help(0);			break;
		case 'k':	global_fvalue = atof(optarg);	break;
		case 'v':	verbosity++;			break;
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "argc %d optind %d\n", argc, optind);
for (foo=0; foo<argc; foo++)
	fprintf(stderr, "%3d  %s\n", foo, argv[foo]);
#endif

if (4 != argc-optind) {
	fprintf(stderr, "%s need some arguments...\n", argv[0]);
	exit(1);
	}

operator = argv[optind+2];
action = look_opcode(operator);
if (action < 0) {
	fprintf(stderr, "%s : opcode '%s' unknow\n", argv[0], operator);
	exit(1);
	}

/*
 *	load the two source files, and check compatibility
 */
if ((foo=fimg_create_from_dump(argv[optind], &srcA))) {
	fprintf(stderr, "read error on '%s' is %d\n", argv[optind], foo);
	exit(2);
	}
if ((foo=fimg_create_from_dump(argv[optind+1], &srcB))) {
	fprintf(stderr, "read error on '%s' is %d\n", argv[optind+1], foo);
	exit(3);
	}

if (verbosity > 1) {				/* please, debug me */
	fimg_describe(&srcA, argv[optind]);
	fimg_describe(&srcB, argv[optind+1]);
	}

foo = fimg_images_not_compatible(&srcA, &srcB);
if (foo) {
	fprintf(stderr, "images are not compatibles, %d\n", foo);
	exit(4);
	}

/*
 *	we can now create the resultant image, and going coredump...
 */
foo = fimg_create(&dest, srcA.width, srcA.height, srcA.type);
if (foo) {
	fprintf(stderr, "%s: crash coredump on create fimg\n", argv[0]);
#if MUST_ABORT
	abort();
#endif
	exit(1);
	}
// fimg_describe(&dest, "destination");

foo = exec_operator(&srcA, &srcB, action, &dest);
if (foo) {
	fprintf(stderr, "operator '%s' exec give us a %d\n",
					operator, foo);
	}

foo = fimg_dump_to_file(&dest, argv[optind+3], 0);
if (foo) {
	fprintf(stderr, "dumping datas to file give us a %d\n", foo);
	}


return 0;
}
/* --------------------------------------------------------------------- */
