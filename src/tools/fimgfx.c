/*
		FIMGFX

 *	some functions here come from 'funcs/contrast.c'
 *
*/
#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>
#include  <unistd.h>

#include  "../floatimg.h"

/* --------------------------------------------------------------------- */

int			verbosity;
float			global_fvalue;

typedef struct {
	char		*name;
	int		id;
	int		nbarg;
	int		flags;
	} Fx;

enum fxid { Fx_cos01=5, Fx_cos010, Fx_pow2, Fx_sqrt, Fx_gray0, Fx_halfsz0,
		Fx_rot90, Fx_cmixa, Fx_desat, Fx_ctr2x2, Fx_norm,
		Fx_classtrial, Fx_mirror, Fx_shift0, Fx_trimul,
		Fx_xper, Fx_binarize, Fx_trinarize,Fx_hilight_R };

Fx fx_list[] = {
	{ "cos01",		Fx_cos01,	0,	1 },
	{ "cos010",		Fx_cos010,	0,	1 },
	{ "pow2",		Fx_pow2,	0,	1 },
	{ "sqrt",		Fx_sqrt,	0,	1 },
	{ "gray0",		Fx_gray0,	0,	1 },
//	{ "halfsz0",		Fx_halfsz0,	0,	1 },
//	{ "rot90",		Fx_rot90,	0,	0 },
	{ "cmixa",		Fx_cmixa,	0,	1 },
	{ "xper",		Fx_xper,	0,	1 },
	{ "desat",		Fx_desat,	0,	1 },
	{ "ctr2x2",		Fx_ctr2x2,	0,	1 },
	{ "mirror",		Fx_mirror,	0,	1 },
	{ "shift0",		Fx_shift0,	0,	1 },
	{ "trimul",		Fx_trimul,	0,	1 },
//	{ "norm",		Fx_norm,	0,	1 },
	{ "classtrial",		Fx_classtrial,	0,	1 },
	{ "binarize",		Fx_binarize,	0,	1 },
	{ "trinarize",		Fx_trinarize,	0,	1 },
	{ "hilightr",		Fx_hilight_R,	0,	1 },
	{ NULL,			0,		0,	0 }
	};

/* --------------------------------------------------------------------- */
static void list_of_effects(void)
{
Fx		*fx;
/* this list must be on just ONE columns */
for (fx=fx_list; fx->name; fx++) {
	printf("%s\n", fx->name);
	}
}
/* --------------------------------------------------------------------- */
int lookup_fxidx(char *txt)
{
Fx		*fx;
int		n;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, txt);
#endif

for (n=0, fx=fx_list; fx->name; fx++, n++) {
#if DEBUG_LEVEL > 1
	fprintf(stderr, "  -> %3d %s\n", n, fx->name);
#endif
	if (!strcmp(fx->name, txt)) {
		return n;
		}
	}
return -1;		/* NOT FOUND */
}
/* --------------------------------------------------------------------- */
/*
 *		this is the mutant function
 */
int do_experiment(FloatImg *S, FloatImg *D, float kf)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %f )\n", __func__, S, D, kf);
#endif

foo = fimg_classif_trial(S, D, kf, 0);
if (foo) {
	fprintf(stderr, "%s error %d classif_trial %p\n", __func__,
				foo, 		S);
	return -98;
	}

return 0;
}
/* --------------------------------------------------------------------- */
static void help(int lvl)
{
Fx		*fx;
int		foo;

printf("-*- fimg special effects -*- %s %s -*-\n", __DATE__, __TIME__);
puts("usage:");
puts("\tfimgfx [options] <effect> source.fimg resultat.fimg");

puts("options:");
puts("\t-k N.N\tset the floating value");
puts("\t-l\tlist effects");
puts("\t-v\tincrease verbosity");

puts("effects:"); 
printf("\t");
foo = 0;
for (fx=fx_list; fx->name; fx++) {
	foo += printf("%s ", fx->name);
	if (foo > 55) {
		printf("\n\t");
		foo = 0;
		}
	}
puts("\n");
fimg_print_version(1);
exit(0);
}
/* --------------------------------------------------------------------- */
int do_an_effect(char *srcfname, int fxidx, char *dstfname)
{
FloatImg	src, dest;
int		foo, action;
double		maxval;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d '%s' )\n", __func__,
			   srcfname, action, dstfname);
#endif

foo = fimg_create_from_dump(srcfname, &src);
if (foo) {
	fprintf(stderr, "err load '%s' : %d\n", srcfname, foo);
	return foo;
	}

maxval = (double)fimg_get_maxvalue(&src);

if (fx_list[fxidx].flags & 1) {
	foo = fimg_clone(&src, &dest, 0);
	if (foo) {
		fprintf(stderr, "err clone %p : %d\n", &src, foo);
		return foo;
		}
	}
else	{
	fprintf(stderr, "%s: ???\n", __func__);		/* XXX */
	memset(&dest, 0, sizeof(dest));
	return -888;
	}

action = fx_list[fxidx].id;

switch (action) {
	case Fx_cos01:
		fimg_cos_01(&src, &dest, maxval);		break;
	case Fx_cos010:
		fimg_cos_010(&src, &dest, maxval);		break;
	case Fx_pow2:
		fimg_power_2(&src, &dest, maxval);		break;
	case Fx_sqrt:
		fimg_square_root(&src, &dest, maxval);		break;

	case Fx_gray0:		/* new 2020 01 10 */
		fimg_to_gray(&src); fimg_copy_data(&src, &dest);
								break;

	case Fx_xper:
		do_experiment(&src, &dest, maxval);		break;
		
	case Fx_rot90:
		foo = fimg_rotate_90(&src, &dest, 0);		break;

	case Fx_cmixa:
		fimg_copy_data(&src, &dest);
		foo = fimg_colors_mixer_a(&dest, 2.0);		break;
		
	case Fx_halfsz0:
		fprintf(stderr, "halfsize was not implemented\n");
		fprintf(stderr, "see 'fimghalfsize.c'. \n");
		return -3;

	case Fx_classtrial:
		fprintf(stderr, "classif trial with %f fvalue\n",
						global_fvalue);
		foo = fimg_classif_trial(&src, &dest, global_fvalue, 0);
		break;

	case Fx_desat:
		fimg_copy_data(&src, &dest);
		foo = fimg_mix_rgb_gray(&dest, global_fvalue);
		break;
	case Fx_mirror:
		foo = fimg_mirror(&src, &dest, 0);
		break;
	case Fx_shift0:
		// fprintf(stderr, "Krkrk %d\n", action);
		foo = fimg_auto_shift_to_zero(&src, &dest);
		break;
	case Fx_trimul:
		fprintf(stderr, "trimul %d\n", action);
		foo = fimg_sfx_triplemul(&src, &dest, 0);
		break;
	case Fx_ctr2x2:
		foo = fimg_contour_2x2(&src, &dest, 0);
		break;
	case Fx_binarize:
		fimg_copy_data(&src, &dest);
		foo = fimg_binarize(&dest, 0);
		break;
	case Fx_trinarize:
		fimg_copy_data(&src, &dest);
		foo = fimg_trinarize(&dest, 0);
		break;
	case Fx_hilight_R:
		foo = fimg_highlight_color(&src, &dest, 'R', 1.333);
		break;
	default:
		fprintf(stderr, "%s %s : %d is bad action\n",
					__FILE__, __func__, action);
		break;

	}

// foo = fimg_export_picture(&dest, dstfname, 0);
foo = fimg_dump_to_file(&dest, dstfname, 0);
if (foo) {
	fprintf(stderr, "dumping datas to file '%s' give us a %d\n",
						dstfname,	foo);
	return foo;
	}

fimg_destroy(&src);
if (dest.type) {
	fimg_destroy(&dest);
	}

return 0;
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt, action;
int		nba;
char		*operator;

char		*srcname = "";
char		*dstname = "out.fimg";

while ((opt = getopt(argc, argv, "hk:lv")) != -1) {
	switch(opt) {
		case 'h':	help(0);			break;
		case 'k':	global_fvalue = atof(optarg);	break;
		case 'l':	list_of_effects();	exit(0);
		case 'v':	verbosity++;			break;
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "argc %d optind %d\n", argc, optind);
for (foo=0; foo<argc; foo++)
	fprintf(stderr, "%3d  %c  %s\n", foo, foo==optind?'*':' ', argv[foo]);
#endif

if (3 > argc-optind) {
	fprintf(stderr, "%s need some arguments...\n", argv[0]);
	exit(1);
	}

if (verbosity>1) fprintf(stderr, "*** fimgfx *** %s %s\n", __DATE__, __TIME__);

operator = argv[optind];
action = lookup_fxidx(operator);
if (action < 0) {
	fprintf(stderr, "garbage found in opcode field : %s\n", operator);
	exit(1);
	}

if (verbosity) {
	fprintf(stderr, "  global fvalue  %f\n", global_fvalue);
	fprintf(stderr, "  action         %d\n", action);
	fprintf(stderr, "  verbosity      %d\n", verbosity);
	}

if ((nba=fx_list[action].nbarg)) {
	fprintf(stderr, "action '%s' need %d arg\n", operator, nba);
	}

srcname = argv[optind+1];
dstname = argv[optind+2];
if (verbosity) fprintf(stderr, "%s  ==>  %s\n", srcname, dstname);

foo = do_an_effect(srcname, action, dstname);
if (foo) {
	fprintf(stderr, "do an effect -> %d\n", foo);
	}

return 0;
}
/* --------------------------------------------------------------------- */
