/*
 *	converting a floatimg to a machinable text file
 *	an ugly software from tTh - february 2021
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <string.h>

#include  "../floatimg.h"

int		verbosity;

/* --------------------------------------------------------------------- */
int export_as_machinable(FloatImg *src, char *fname, int steps, int flags)
{
FILE		*fp;
int		x, y;
float		rgb[3];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %25s ( %p '%s' %d )\n", __func__,
			src, fname, flags);
#endif

fp = NULL;					/* molly guard */
if (strcmp("-", fname)) {			/* real file */
	fprintf(stderr, "real file '%s'\n", fname);
	}
else {
	// fprintf(stderr, "kitchen sink\n");
	}

fp = stdout;					/* XXX */
for (y=0; y<src->height; y+=steps) {
	for (x=0; x<src->width; x+=steps) {
		fimg_get_rgb(src, x, y, rgb);
		fprintf(fp, "%d %d ", x, y);
		fprintf(fp, "%f %f %f\n", rgb[0], rgb[1], rgb[2]);
		}
	}

return 0;
}
/* --------------------------------------------------------------------- */
static int normalize(FloatImg *pimg, float vmax)
{
float		mmv[6], maxi, coef;
int		foo, sz, idx;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %g )\n", __func__, pimg, vmax);
#endif

foo = fimg_get_minmax_rgb(pimg, mmv);
if (foo) {
	fprintf(stderr, "%s: ABEND\n", __func__);
	abort();
	}
			maxi = mmv[1];
if (mmv[3] > maxi)	maxi = mmv[3];
if (mmv[5] > maxi)	maxi = mmv[5];
coef = vmax / maxi;
if (verbosity) {
	fprintf(stderr, "mins %f %f %f\n", mmv[0], mmv[2], mmv[4]);
	fprintf(stderr, "maxs %f %f %f\n", mmv[1], mmv[3], mmv[5]);
	fprintf(stderr, "coef    = %f\n", coef);
	}

sz = pimg->width * pimg->height;

for (idx=0; idx<sz; idx++) {
	pimg->R[idx] *= coef;
	pimg->G[idx] *= coef;
	pimg->B[idx] *= coef;
	}

return 0;
}
/* --------------------------------------------------------------------- */
int convertir_fimg_en_machinable(char *srcname, char *dstname,
					int steps, float norm)
{
int		foo, infos[3];
FloatImg	fimg;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %25s ( '%s' '%s' %d )\n", __func__,
			srcname, dstname, notused);
#endif

if (steps < 1) {
	fprintf(stderr, "%s: steps MUST be > 0\n", __func__);
	exit(1);
	}

foo = fimg_fileinfos(srcname, infos);
if (foo) {
	fprintf(stderr, "'%s' get dims -> %d\n", srcname, foo);
	return foo;
	}

if (verbosity) {
	fprintf(stderr, "%s: image '%s' is %d x %d  %s\n", 
				__func__,
				srcname, infos[0], infos[1],
				fimg_str_type(infos[2]));
	}

foo = fimg_create_from_dump(srcname, &fimg);
if (foo) {
	fprintf(stderr, "create fimg from '%s' -> %d\n", srcname, foo);
	return -1;
	}

if (verbosity) {
	fimg_describe(&fimg, srcname);
	fprintf(stderr, "normalize to %f\n", norm);
	}
if (norm > 0.0) {
	// fprintf(stderr, "normalize %p\n", &fimg);
	foo = normalize(&fimg, norm);
	}

foo = export_as_machinable(&fimg, dstname, steps, 0);
if (foo) {
	fprintf(stderr,"%s: err %d on export\n", __func__, foo);
	}
fimg_destroy(&fimg);

return 0;
}
/* --------------------------------------------------------------------- */
void help(int k)
{
puts("usage:\n\tfimg2text [options] foo.fimg > bar.csv");
puts("options:");
puts("\t-v\t\tincrease verbosity");
puts("\t-n 3.14\t\tnormalize picture");
puts("\t-s N\t\tsteps on x & y");
if (verbosity) fimg_print_version(1);
exit(0);
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		foo, opt;
int		steps = 1;
float		norm_val = 222.0;	/* < 0 : don't normalize */
char		separator = ' ';

while ((opt = getopt(argc, argv, "f:hn:s:v")) != -1) {
	switch(opt) {
		case 'f':	separator = optarg[0];		break;
		case 'v':	verbosity++;			break;
		case 'h':	help(1);			exit(1);
		case 's':	steps = atoi(optarg);		break;
		case 'n':	norm_val = atof(optarg);	break;
		}
	}

if (1 != argc-optind) {
	fprintf(stderr, "error: %s need one intput filename\n", argv[0]);
	exit(1);
	}

foo = convertir_fimg_en_machinable(argv[optind], "-", steps, norm_val);
if (foo) {
	fprintf(stderr, "%s : got a %d from convertor\n", argv[0], foo);
	return 1;
	}

return 0;
}
/* --------------------------------------------------------------------- */

