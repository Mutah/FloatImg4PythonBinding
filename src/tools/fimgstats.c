/*
 *		FIMGSTATS
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>

#include  "../floatimg.h"

int		verbosity;		/* global */

int		make_csv;

/* --------------------------------------------------------------------- */
int various_numbers(FloatImg *fimg, int k)
{
float		moyennes[4];
int		foo;
// float		fvalue;
float		vals[6];

if (verbosity) {
	fprintf(stderr, "     numbers from %p :\n", fimg); 
	}

fimg_printhead(fimg);
fprintf(stderr, "surface      %d\n", fimg->width * fimg->height);

fimg_meanvalues(fimg, moyennes);
fprintf(stderr, "mean values:\n");
for (foo=0; foo<4; foo++)
	printf("      %c   %14.6f\n", "RGBA"[foo], moyennes[foo]);

foo = fimg_count_negativ(fimg);
if (foo) {
	fprintf(stderr, "%d negative values\n", foo);
	}

foo = fimg_get_minmax_rgb(fimg, vals);
if (foo) {
	fprintf(stderr, "%s: err %d on fimg_get_minmax_rgb\n",
			__func__, foo);
	return foo;
	}

printf("Rmin  %12.4g    Rmax  %12.4g    delta %12g\n",
			vals[0], vals[1], vals[1]-vals[0]);
printf("Gmin  %12.4g    Gmax  %12.4g          %12g\n",
			vals[2], vals[3], vals[3]-vals[2]);
printf("Bmin  %12.4g    Bmax  %12.4g          %12g\n",
			vals[4], vals[5], vals[5]-vals[4]);

return 0;
}
/* --------------------------------------------------------------------- */
int various_numbers_from_file(char *fname, int k)
{
FloatImg	fimg;
int		foo;

fprintf(stderr, "------ numbers from '%s' :\n", fname); 

foo = fimg_create_from_dump(fname, &fimg);
if (foo) {
	fprintf(stderr, "create fimg from '%s' -> %d\n", fname, foo);
	return -2;
	}

various_numbers(&fimg, k);

fimg_destroy(&fimg);

return 0;
}
/* --------------------------------------------------------------------- */
static void help(int k)
{
fputs(	"usage : fimgstats [options] file.fimg\n"
	"\t-c\tmake a machinable csv\n"
	"\t-v\tincrease verbosity\n"
	, stderr);
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt;

extern char	*optarg;
extern int	optind, opterr, optopt;

if (argc == 1) {
	foo = fimg_print_version(1); help(0);
	exit(0);
	}

while ((opt = getopt(argc, argv, "chv")) != -1) {
	switch(opt) {
		case 'c':	make_csv++;		break;
		case 'v':	verbosity++;		break;

		case 'h':	/* tombe dedans */
		default:	help(1);		exit(1);
		}
	}

if (NULL==argv[optind]) {
	fprintf(stderr, "optind %d is wtf\n", optind);
	return 1;
	}
foo = various_numbers_from_file(argv[optind], 0);
if (foo) {
	fprintf(stderr, "got a %d ?\n", foo);
	}
return 0;
}
/* --------------------------------------------------------------------- */
