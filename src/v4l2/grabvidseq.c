   /* V4L2 video picture grabber

		Origin :V4L2GRAB.C - patched by tTh

       Copyright (C) 2009 Mauro Carvalho Chehab <mchehab@infradead.org>

       This program is free software; you can redistribute it and/or modify
       it under the terms of the GNU General Public License as published by
       the Free Software Foundation version 2 of the License.

       This program is distributed in the hope that it will be useful,
       but WITHOUT ANY WARRANTY; without even the implied warranty of
       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
       GNU General Public License for more details.
     */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <libv4l2.h>

#include "../floatimg.h"
#include  "funcs.h"

/* --------------------------------------------------------------------- */
/*	compilation control */

#define SAVE_AS_CUMUL	1
#define SAVE_AS_FIMG	0

#define NBR_BUFFERS	4

/* --------------------------------------------------------------------- */

#define CLEAR(x) memset(&(x), 0, sizeof(x))

struct buffer {
	void   *start;
	size_t length;
	};

int			verbosity;
static int		systrace;

/* --------------------------------------------------------------------- */

static void xioctl(int fh, int request, void *arg)
{
int	r;

/* may be imagine a system for displaying all call to this function ? */
if (systrace) {
	fprintf(stderr, "xioctl fd=%d req=%d arg=%p\n", fh, request, arg);
	}

do	{
	r = v4l2_ioctl(fh, request, arg);
	} while (r == -1 && ((errno == EINTR) || (errno == EAGAIN)));

if (r == -1) {
	fprintf(stderr, "error %d, %s\n", errno, strerror(errno));
	sleep(1);
	exit(EXIT_FAILURE);
	}
}
/* --------------------------------------------------------------------- */
void help(int v)
{
if (verbosity) {
	printf("compiled %s at %s\n", __DATE__, __TIME__);
	fimg_print_version(1);
	}
puts("options :");
puts("\t-d /dev/?\tselect video device");
puts("\t-g\t\tconvert to gray");
puts("\t-n NNN\t\thow many frames ?");
puts("\t-O ./\t\tset Output dir");
puts("\t-o bla.xxx\tset output filename");
puts("\t-p NN.N\t\tperiod in seconds");
puts("\t-r NNN\t\trotate picture");
puts("\t-s WxH\t\tsize of capture");
puts("\t-c mode\t\tcontrast enhancement");
puts("\t-u\t\ttry upscaling...");
puts("\t-v\t\tincrease verbosity");
puts("\t-Z\t\tenable systrace");
if (verbosity) {
	puts("\n\t\tXXX list all the contrast modes, please\n");
	}
exit(0);
}
/* --------------------------------------------------------------------- */

int main(int argc, char **argv)
{
struct v4l2_format              fmt;
struct v4l2_buffer              buf;
struct v4l2_requestbuffers      req;
enum v4l2_buf_type              type;
fd_set                          fds;
struct timeval                  tv;
int                             r, fd = -1;
unsigned int                    i, n_buffers;
char                            *dev_name = "/dev/video0";

// XXX FILE                            *fout;
struct buffer                   *buffers;

int		foo;
double		period = 10.0;		/* delai entre les captures
					   en secondes */
int		nbre_capt = 1;		/* nombre de captures */
int		opt;
int		width = 640;
int		height = 480;
double		t_final, maxvalue;
int		to_gray = 0;
int		upscaling = 0;
int		contrast = CONTRAST_NONE;
int		rotfactor = 0;			/* only 0 or 90 here */
char		*dest_dir = ".";		/* no trailing slash */
char		*outfile = "out.pnm";

#if SAVE_AS_CUMUL
FloatImg	cumul, tmpfimg, *to_save;
#endif

while ((opt = getopt(argc, argv, "c:d:ghn:o:O:p:r:s:uvZ")) != -1) {
	switch(opt) {
		case 'c':	contrast = fimg_id_contraste(optarg);
				if (contrast < 0) {
					fputs("unknow contrast\n", stderr);
					exit(1);
					}
				break;
		case 'd':	dev_name = optarg;		break;
		case 'g':	to_gray = 1;			break;
		case 'h':	help(0);			break;
		case 'n':	nbre_capt = atoi(optarg);	break;
		case 'O':	dest_dir = optarg;		break;
		case 'o':	outfile = optarg;		break;
		case 'p':	foo = parse_double(optarg, &period);
				if (foo<0) {
					fprintf(stderr,
						"error parsing -p arg '%s'\n",
						optarg);
					exit(1);
					}
				break;
		case 'r':	rotfactor = atoi(optarg);	break;
		case 's':	parse_WxH(optarg, &width, &height);
				break;
		case 'u':	upscaling = 1;			break;
		case 'v':	verbosity++;			break;
		case 'Z':	systrace = 1;			break;
		default:
			fprintf(stderr, "option '%c' is wtf\n", opt);
			exit(1);
		}
	}

if (verbosity > 1) {
	fprintf(stderr, "*** GrabVidSeq (%s, %s) libv %d, pid=%d\n",
				__DATE__, __TIME__, FIMG_VERSION, getpid());
	fprintf(stderr, "grabing %d picz, ", nbre_capt);
	fprintf(stderr, "period is %.3f seconds\n", period);
	fprintf(stderr, "framesize is %dx%d\n", width, height);
	// fprintf(stderr, "destdir is '%s'\n", dest_dir);
	if (upscaling) fprintf(stderr, "upscaling is on\n");
	}

if (upscaling && (nbre_capt%4)) {
	fprintf(stderr, "WARN upscaling: %d bad nbre_capt\n",
					   nbre_capt);
	}

fd = v4l2_open(dev_name, O_RDWR | O_NONBLOCK, 0);
if (fd < 0) {
	perror(dev_name);
	exit(EXIT_FAILURE);
	}

CLEAR(fmt);
fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
fmt.fmt.pix.width       = width;
fmt.fmt.pix.height      = height;
fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;
xioctl(fd, VIDIOC_S_FMT, &fmt);
if (fmt.fmt.pix.pixelformat != V4L2_PIX_FMT_RGB24) {
	/* are others formats usable ? */
	fprintf(stderr, "Libv4l didn't accept RGB24 format. Can't proceed.\n");
	exit(EXIT_FAILURE);
	}
if ((fmt.fmt.pix.width != width) || (fmt.fmt.pix.height != height)) {
	fprintf(stderr, "Warning: driver is sending image at %dx%d\n",
			    fmt.fmt.pix.width, fmt.fmt.pix.height);
	}

// fprintf(stderr,"--- Ok 1\n");

CLEAR(req);
req.count = NBR_BUFFERS;
req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
req.memory = V4L2_MEMORY_MMAP;
xioctl(fd, VIDIOC_REQBUFS, &req);

buffers = calloc(req.count, sizeof(*buffers));
for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
	CLEAR(buf);

	buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory      = V4L2_MEMORY_MMAP;
	buf.index       = n_buffers;

	xioctl(fd, VIDIOC_QUERYBUF, &buf);

	buffers[n_buffers].length = buf.length;
	buffers[n_buffers].start = v4l2_mmap(NULL, buf.length,
				PROT_READ | PROT_WRITE, MAP_SHARED,
				fd, buf.m.offset);

	if (MAP_FAILED == buffers[n_buffers].start) {
		perror("v4l2_mmap");
		exit(EXIT_FAILURE);
		}
	}

(void)fimg_timer_set(0);

for (i = 0; i < NBR_BUFFERS; ++i) {
	CLEAR(buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	buf.index = i;
	xioctl(fd, VIDIOC_QBUF, &buf);
	}

#if SAVE_AS_CUMUL
if (upscaling) {
	foo = fimg_create(&cumul,
			fmt.fmt.pix.width*2, fmt.fmt.pix.height*2,
			FIMG_TYPE_RGB);
	}
else	{
	foo = fimg_create(&cumul,
			fmt.fmt.pix.width, fmt.fmt.pix.height,
			FIMG_TYPE_RGB);
	}
fimg_clear(&cumul);
cumul.fval = 255.0;		/* must be read from camera XXX */
cumul.count = 0;
to_save = &cumul;
#endif

type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
xioctl(fd, VIDIOC_STREAMON, &type);

#if 1
if (verbosity)	fprintf(stderr,"pid %d is going to grab %d picz...\n",
					getpid(), nbre_capt);
#endif

/*
 *		START ON THE GRABBING LOOP
 */
for (i = 0; i < nbre_capt; i++) {
	do	{
		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		/* Timeout. */
		tv.tv_sec = 2;
		tv.tv_usec = 0;

		r = select(fd + 1, &fds, NULL, NULL, &tv);
		} while ((r == -1 && (errno = EINTR)));

	if (r == -1) {
		perror("select");
		return errno;			/* WTF ? a rogue return
						   from the main() ? */
		}

	if(verbosity > 1) {
		fprintf(stderr, "%6d / %6d      %9.3f\r", i, nbre_capt,
						fimg_timer_get(0));
		fflush(stderr);
		}
	CLEAR(buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	xioctl(fd, VIDIOC_DQBUF, &buf);
	if(verbosity > 2) {
		fprintf(stderr, "xioctl VIDIOC_DQBUF done\n");
		fflush(stderr);
		}

#if SAVE_AS_CUMUL
	if (upscaling) {
		x_upscaler_0(buffers[buf.index].start,
			fmt.fmt.pix.width, fmt.fmt.pix.height, &cumul);
		}
	else	{
		x_add_rgb2fimg(buffers[buf.index].start,
			fmt.fmt.pix.width, fmt.fmt.pix.height, &cumul);
		}
#endif

#if SAVE_AS_FIMG
	sprintf(out_name, "%s/%05d.fimg", dest_dir, i);
	if (verbosity > 1) fprintf(stderr, "--> %s\n", out_name);
	foo = x_rgb2file(buffers[buf.index].start,
			fmt.fmt.pix.width, fmt.fmt.pix.height,
			out_name);
#endif

	if (nbre_capt > 1 && period > 0.001) {
		/* suspend execution for
		   microsecond intervals */
		usleep((int)(period*1E6));
		}

	xioctl(fd, VIDIOC_QBUF, &buf);
	}

if (verbosity) {
	t_final = fimg_timer_get(0);
	fprintf(stderr, "pid %d : elapsed %.3g s -> %.2f fps\n", getpid(),
			t_final, (double)nbre_capt / t_final);
	}

if (to_gray) {
	if (verbosity)	fputs("converting to gray\n", stderr);
	foo = fimg_to_gray(&cumul);
	}


#if SAVE_AS_CUMUL
	// save cumul to file
if (verbosity) fprintf(stderr, "saving cumul to '%s'\n", outfile);

/* ----- nouveau 15 nov 2019 */
maxvalue = cumul.fval * cumul.count;
if (verbosity) {
	fprintf(stderr, "theorical maxvalue = %g\n", maxvalue);
	fprintf(stderr, "computed max value = %g\n",
					fimg_get_maxvalue(&cumul));
	}
switch (contrast) {
	case CONTRAST_NONE:
		// if (verbosity) fprintf(stderr, "contrast: none\n");
		break;
	case CONTRAST_SQRT:
		fimg_square_root(&cumul, NULL, maxvalue);
		break;
	case CONTRAST_POW2:
		fimg_power_2(&cumul, NULL, maxvalue);
		break;
	case CONTRAST_COS01:
		fimg_cos_01(&cumul, NULL, maxvalue);
		break;
	case CONTRAST_COS010:
		fimg_cos_010(&cumul, NULL, maxvalue);
		break;
	default:
		fprintf(stderr, "bad contrast method\n");
		break;
	}

/* XXX warning, new from coronahome 26 mars 2020 */
to_save = &cumul;
if (90 == rotfactor) {
	memset(&tmpfimg, 0, sizeof(FloatImg));
	foo = fimg_rotate_90(&cumul, &tmpfimg, 0);
	if (verbosity > 2) {
		fprintf(stderr, "dump rot90 %p\n", &tmpfimg);
		foo = fimg_save_as_png(&tmpfimg, "rot90.png", 0);
		}
	to_save = &tmpfimg;
	}


foo = format_from_extension(outfile);
switch (foo) {
	case FILE_TYPE_FIMG:
		foo = fimg_dump_to_file(to_save, outfile, 0);
		break;
	case FILE_TYPE_PNM:
		foo = fimg_save_as_pnm(to_save, outfile, 1);
		break;
	case FILE_TYPE_PNG:
		foo = fimg_save_as_png(to_save, outfile, 0);
		break;
	case FILE_TYPE_FITS:
		foo = fimg_save_R_as_fits(to_save, outfile, 0);
		break;
	case FILE_TYPE_TIFF:
		foo = fimg_write_as_tiff(to_save, outfile, 0);
		break;
	default:
		fprintf(stderr, "can't save as %s\n", outfile);
		break;
	}
	// free buffers

fimg_destroy(&cumul);
#endif

type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
xioctl(fd, VIDIOC_STREAMOFF, &type);
for (i = 0; i < NBR_BUFFERS; ++i) {
	v4l2_munmap(buffers[i].start, buffers[i].length);
	}

// free(buffers);			/* atomic bombing */

v4l2_close(fd);

return 0;
}
