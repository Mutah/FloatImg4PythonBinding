/*
 *	fonctions pour afficher les structures de V4L2
 */

#include  <stdio.h>
#include  <inttypes.h>
#include  <linux/videodev2.h>

#include  "v4l2_pr_structs.h"

#define		FP	(stdout)

extern int	verbosity;

/* --------------------------------------------------------------------- */
static char *fmttype2str(int type)
{

switch(type) {
	case 0:		return "[zero]";
	case V4L2_BUF_TYPE_VIDEO_CAPTURE:	return "video capture";
	case V4L2_BUF_TYPE_VIDEO_OUTPUT:	return "video output";

	case 13:	return "META capture";
	}

return "XXX";
}
/* --------------------------------------------------------------------- */
static void pr_capabilities(uint32_t caps)
{
fputs("     ", FP);

if (caps & V4L2_CAP_VIDEO_CAPTURE)	fputs("vcapt ", FP);
if (caps & V4L2_CAP_VIDEO_OUTPUT)	fputs("vout ", FP);
if (caps & V4L2_CAP_VIDEO_OVERLAY)	fputs("overlay ", FP);
if (caps & V4L2_CAP_VBI_CAPTURE)	fputs("vbicapt ", FP);
if (caps & V4L2_CAP_VBI_OUTPUT)		fputs("vbiout ", FP);

		/* to be continued */

if (caps & V4L2_CAP_AUDIO)		fputs("audio ", FP);


if (caps & V4L2_CAP_SDR_CAPTURE)	fputs("sdrcapt ", FP);
if (caps & V4L2_CAP_EXT_PIX_FORMAT)	fputs("extpix ", FP);
if (caps & V4L2_CAP_SDR_OUTPUT)		fputs("sdrout ", FP);




if (caps & V4L2_CAP_READWRITE)		fputs("rwsysc ", FP);
if (caps & V4L2_CAP_ASYNCIO)		fputs("asyncio ", FP);
if (caps & V4L2_CAP_STREAMING)		fputs("stream ", FP);


fputs("\n", FP);
}
/* --------------------------------------------------------------------- */
int pr_v4l2_capability(char *txt, struct v4l2_capability *ptr)
{
fprintf(FP, "## v4l2_capability, %-15s  %p\n", txt, ptr);

fprintf(FP, "   driver          %s\n",   ptr->driver);
fprintf(FP, "   card            %s\n",   ptr->card);
fprintf(FP, "   bus info        %s\n",   ptr->bus_info);

fprintf(FP, "   version         0x%X\n", ptr->version);
fprintf(FP, "   capabilities    0x%X\n", ptr->capabilities);
pr_capabilities(ptr->capabilities);
fprintf(FP, "   device caps     0x%X\n", ptr->device_caps);

return -1;
}
/* --------------------------------------------------------------------- */
/*
 *	warning, this function return a pointer to a static
 *	strint, so it was NOT reentrant !
 */
char *str_fourcc(uint32_t fcc)
{
static char chaine[10];

chaine[0] = '['; chaine[5] = ']'; chaine[6] = '\0';
chaine[1] = (fcc>>0)   & 0xff;
chaine[2] = (fcc>>8)   & 0xff;
chaine[3] = (fcc>>16)  & 0xff;
chaine[4] = (fcc>>24)  & 0xff;

return chaine;
}
/* --------------------------------------------------------------------- */
char *str_buf_type(int type)
{
switch (type) {
	case V4L2_BUF_TYPE_VIDEO_CAPTURE:	return "vidcapt";
	case V4L2_BUF_TYPE_VIDEO_OUTPUT:	return "vidout";
	case V4L2_BUF_TYPE_VIDEO_OVERLAY:        return "vidovrl";
	case V4L2_BUF_TYPE_VBI_CAPTURE:          return "vbicapt";
	case V4L2_BUF_TYPE_VBI_OUTPUT:           return "vbicapt";
	case V4L2_BUF_TYPE_SLICED_VBI_CAPTURE:   return "slicevcapt";
	case V4L2_BUF_TYPE_SLICED_VBI_OUTPUT:    return "slicevout";
	case V4L2_BUF_TYPE_VIDEO_OUTPUT_OVERLAY: return "v-outover";
	case V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE: return "v-captmpla";
	case V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE:  return "v-outmpla";
	case V4L2_BUF_TYPE_SDR_CAPTURE:          return "sdrcapt";
	case V4L2_BUF_TYPE_SDR_OUTPUT:           return "sdrout";
	/* Deprecated, do not use */
	case V4L2_BUF_TYPE_PRIVATE:              return "private";

	}

return "???";
}
/* --------------------------------------------------------------------- */
int pr_v4l2_fmtdesc(char *txt, struct v4l2_fmtdesc *ptr)
{
fprintf(FP, "## v4l2_fmtdesc, %-15s  %p\n", txt, ptr);

fprintf(FP, "   index           %d\n", ptr->index);
fprintf(FP, "   type            %d\n", ptr->type); /* enum v4l2_buf_type */
fprintf(FP, "   flags           0x%X\n", ptr->flags); 
fprintf(FP, "   description     %s\n", ptr->description); 
fprintf(FP, "   pixel format    0x%X\n", ptr->pixelformat); 	/* FOURCC */

return -1;
}
/* --------------------------------------------------------------------- */
char *str_input_type(int t)
{
switch (t) {
	case V4L2_INPUT_TYPE_TUNER:	return "tuner";
	case V4L2_INPUT_TYPE_CAMERA:	return "camera";
	case V4L2_INPUT_TYPE_TOUCH:	return "touch";
	}
return "???";
}
/* --------------------------------------------------------------------- */
static void pr_input_status(uint32_t st)
{
if (st & V4L2_IN_ST_NO_POWER)		fputs("nopow ", FP);
if (st & V4L2_IN_ST_NO_SIGNAL)		fputs("nosig ", FP);
if (st & V4L2_IN_ST_NO_COLOR)		fputs("nocol ", FP);

if (st & V4L2_IN_ST_HFLIP)		fputs("hflip ", FP);
if (st & V4L2_IN_ST_VFLIP)		fputs("vflip ", FP);

if (st & V4L2_IN_ST_NO_H_LOCK)		fputs("nohlck ", FP);
if (st & V4L2_IN_ST_COLOR_KILL) 	fputs("colkil ", FP);
if (st & V4L2_IN_ST_NO_V_LOCK)		fputs("novlck ", FP);
if (st & V4L2_IN_ST_NO_STD_LOCK)	fputs("nostdlk ", FP);

if (st & V4L2_IN_ST_NO_EQU)		fputs("noequ ", FP);
if (st & V4L2_IN_ST_NO_CARRIER)		fputs("nocarr ", FP);

if (st & V4L2_IN_ST_MACROVISION)	fputs("macrov ", FP);
if (st & V4L2_IN_ST_NO_ACCESS)		fputs("noacces ", FP);
if (st & V4L2_IN_ST_VTR)		fputs("VTR ", FP);

		/* to be continued ?*/
}
/* --------------------------------------------------------------------- */
static void pr_input_capabilities(uint32_t st)
{
if (st & V4L2_IN_CAP_DV_TIMINGS)	fputs("DVtime ", FP);
if (st & V4L2_IN_CAP_STD)		fputs("s_std ", FP);
if (st & V4L2_IN_CAP_NATIVE_SIZE)	fputs("nativsz ", FP);

}
/* --------------------------------------------------------------------- */
int pr_v4l2_input(char *txt, struct v4l2_input *ptr)
{
fprintf(FP, "## v4l2_input, %-15s  %p\n", txt, ptr);

fprintf(FP, "   index           %d\n",   ptr->index);
fprintf(FP, "   name            %s\n",   ptr->name);
fprintf(FP, "   type            %d    %s\n", ptr->type,
					str_input_type(ptr->type));
fprintf(FP, "   audioset        0x%X\n", ptr->audioset);
fprintf(FP, "   tuner           0x%X\n", ptr->tuner);
/* XXX				v4l2_std_id  std; */
fprintf(FP, "   status          0x%X\n",   ptr->status);
if (ptr->status) {
	fputs("     ",FP);
	pr_input_status(ptr->status);
	fputs("\n",FP);
	}
fprintf(FP, "   capabilities    0x%X\n", ptr->capabilities);
if (ptr->capabilities) {
	fputs("     ",FP);
	pr_input_capabilities(ptr->capabilities);
	fputs("\n",FP);
	}

return -1;
}
/* --------------------------------------------------------------------- */
char * str_colorspace(int colspace)
{
switch(colspace) {
	case V4L2_COLORSPACE_DEFAULT:		return "default";
	case V4L2_COLORSPACE_SMPTE170M:		return "smpte170m";
	case V4L2_COLORSPACE_SMPTE240M:		return "smpte240m";
	case V4L2_COLORSPACE_REC709:		return "rec709";
	case V4L2_COLORSPACE_BT878:		return "bt878";
	case V4L2_COLORSPACE_470_SYSTEM_M:	return "470-sys-M";
	case V4L2_COLORSPACE_470_SYSTEM_BG:	return "470-sys-BG";
	case V4L2_COLORSPACE_JPEG:		return "jpeg";
	case V4L2_COLORSPACE_SRGB:		return "srgb";
	case V4L2_COLORSPACE_ADOBERGB:		return "adobergb";
	case V4L2_COLORSPACE_BT2020:		return "bt2020";
	case V4L2_COLORSPACE_RAW:		return "raw";
	case V4L2_COLORSPACE_DCI_P3:		return "dci-p3";
	}

return "???";
}
/* --------------------------------------------------------------------- */
int pr_v4l2_format(char *txt, struct v4l2_format *ptr)
{
fprintf(FP, "## v4l2_format, %-15s  %p\n", txt, ptr);
fprintf(FP, "   type %d          %s\n", ptr->type,/* enum v4l2_buf_type */
			fmttype2str(ptr->type));

switch(ptr->type) {

	case V4L2_BUF_TYPE_VIDEO_CAPTURE:
		fprintf(FP, "   dims            %dx%d\n",
				ptr->fmt.pix.width,
				ptr->fmt.pix.height);
		fprintf(FP, "   pixformat       %s\n",
				str_fourcc(ptr->fmt.pix.pixelformat));
		fprintf(FP, "   field           %d\n", 
				ptr->fmt.pix.field); /* enum v4l2_field */
		fprintf(FP, "   padding         %d\n",
				ptr->fmt.pix.bytesperline);
		fprintf(FP, "   sizeimage       %d\n",
				ptr->fmt.pix.sizeimage);
		fprintf(FP, "   colorspace      %d  %s\n",
				ptr->fmt.pix.colorspace,
				str_colorspace(ptr->fmt.pix.colorspace));
		break;

	default:	fprintf(FP, "XXX type %d unknow\n", ptr->type);
			break;
	
	}
return 0;
}
/* --------------------------------------------------------------------- */
int pr_v4l2_requestbuffers(char *txt, struct v4l2_requestbuffers *ptr)
{
fprintf(FP, "## v4l2_requestbuffers, %s %p\n", txt, ptr);
fprintf(FP, "   type %d\n", ptr->type);		/* enum v4l2_buf_type */


return 0;
}
/* --------------------------------------------------------------------- */
char *str_ctrl_type(int type)
{
switch (type) {
        case V4L2_CTRL_TYPE_INTEGER:		return "integer";
        case V4L2_CTRL_TYPE_BOOLEAN:		return "boolean";
        case V4L2_CTRL_TYPE_MENU:		return "menu";
        case V4L2_CTRL_TYPE_BUTTON:		return "button";
        case V4L2_CTRL_TYPE_INTEGER64:		return "int64";
        case V4L2_CTRL_TYPE_CTRL_CLASS:		return "ctrl-class";
        case V4L2_CTRL_TYPE_STRING:		return "string";
        case V4L2_CTRL_TYPE_BITMASK:		return "bitmask";
        case V4L2_CTRL_TYPE_INTEGER_MENU:	return "int-menu";
	}

return "???";
}
/* --------------------------------------------------------------------- */
/*
 The 32-bit qctrl.id value is subdivided into three bit ranges:
 the top 4 bits are reserved for flags (e. g. V4L2_CTRL_FLAG_NEXT_CTRL)
 and are not actually part of the ID.
 The remaining 28 bits form the control ID, of which the most significant
 12 bits define the control class and the least significant
 16 bits identify the control within the control class. 
 */
void pr_ctrl_id(uint32_t id)
{

if (verbosity>1) fprintf(FP, "%08x : ", id);

fprintf(FP, "%x %03lx %04x", (id>>28)&0xf,
		V4L2_CTRL_ID2CLASS(id)>>16, id&0xffff);

}
/* --------------------------------------------------------------------- */
int pr_v4l2_control(char *txt, struct v4l2_control *ptr)
{

return -1;
}
/* --------------------------------------------------------------------- */
