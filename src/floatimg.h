/*
 *			floatimg.h
 *			ugly code from tTh
 *	http://la.buvette.org/photos/cumul
 */

#define		FIMG_VERSION	150

/*
 *	in memory descriptor
 */
#define MAGIC_FIMG	0x00F11F00
typedef struct {
	unsigned long	magic;
	int		width;
	int		height;
	int		type;
	float		fval;
	int		count;
	float		*R, *G, *B, *A;
	int		reserved;
	} FloatImg;

/*
 *	fimg file header
 */
typedef struct {
	char		magic[8];
	int32_t		w, h, t;
	} FimgFileHead;

#define MAGIC_AREA51	0xA5EA0051
typedef struct {
	unsigned long	magic;
	int		w, h;
	int		x, y;
	int		flags;
	} FimgArea51;

#define FIMG_TYPE_GRAY			1
#define FIMG_TYPE_RGB			3
#define FIMG_TYPE_RGBA			4
#define FIMG_TYPE_RGBZ			99

#define FILE_TYPE_FIMG			1
#define FILE_TYPE_PNM			2
#define FILE_TYPE_PNG			3
#define FILE_TYPE_TGA			4
#define FILE_TYPE_TIFF			5
#define FILE_TYPE_FITS			6
#define FILE_TYPE_BMP			7
#define FILE_TYPE_EXR			8

/*	lib/contrast.c			*/
#define CONTRAST_NONE			0
#define CONTRAST_SQRT			1
#define CONTRAST_POW2			2
#define CONTRAST_COS01			3
#define CONTRAST_COS010			4

/*
 *	core module
 */
int   fimg_create(FloatImg *fimg, int w, int h, int t);
int   fimg_destroy(FloatImg *fimg);
int   fimg_clone(FloatImg *fimg, FloatImg *newpic, int flags);
int   fimg_copy_data(FloatImg *from, FloatImg *to);
int   fimg_type_is_valid(int type);


int   fimg_print_version(int k);
void  fimg_print_sizeof(void);
void  fimg_printhead(FloatImg *h);
void  fimg_printdims(char *txt, FloatImg *pi);
int   fimg_describe(FloatImg *head, char *txt);
char *fimg_str_type(int type);
int   fimg_plot_rgb (FloatImg *head, int x, int y,
					float r, float g, float b);
int   fimg_get_rgb(FloatImg *head, int x, int y, float *rgb);
int   fimg_put_rgb(FloatImg *head, int x, int y, float *rgb);
int   fimg_clear(FloatImg *fimg);
int   fimg_add_rgb(FloatImg *head, int x, int y, float r, float g, float b);
int   fimg_rgb_constant(FloatImg *head, float r, float g, float b);

/*	--> lib/fimg-compare.c		*/
int fimg_images_not_compatible(FloatImg *a, FloatImg *b);

int fimg_interpolate(FloatImg *s1, FloatImg *s2, FloatImg *d, float coef);

/*	'operats' module 		*/
int fimg_add_3(FloatImg *a, FloatImg *b, FloatImg *d);
int fimg_add_2(FloatImg *a, FloatImg *b);			/* B+=A */
int fimg_sub_3(FloatImg *a, FloatImg *b, FloatImg *d);
int fimg_mul_3(FloatImg *a, FloatImg *b, FloatImg *d);
int fimg_minimum(FloatImg *a, FloatImg *b, FloatImg *d);
int fimg_maximum(FloatImg *a, FloatImg *b, FloatImg *d);

/*	funcs/filtrage.c		*/

typedef struct {
	float		matrix[9];
	float		mult;
	float		offset;
	} FimgFilter3x3;

int fimg_killborders(FloatImg *img);
int fimg_lissage_2x2(FloatImg *img);
int fimg_filter_3x3(FloatImg *s, FloatImg *d, FimgFilter3x3 *filtr);


int fimg_contour_2x2(FloatImg *psrc, FloatImg *pdst, int reverse);

/*	 module sfx0.c			*/
int fimg_killcolors_a(FloatImg *fimg, float fval);
int fimg_killcolors_b(FloatImg *fimg, float fval);
int fimg_colors_mixer_a(FloatImg *fimg, float fval);

/*	 module sfx1.c			*/
int fimg_highlight_color(FloatImg *src, FloatImg *dst,
					char color, float fval);

/*	 module sfx2.c			*/
int fimg_binarize(FloatImg *pimg, int notused);
int fimg_trinarize(FloatImg *pimg, int notused);

/*	 module sfx4.c			*/
int fimg_sfx_triplemul(FloatImg *s, FloatImg *d, int notused);

/*	funcs/rotate.c module		*/
/*	#coronamaison			*/
int fimg_rotate_90(FloatImg *src, FloatImg *dst, int notused);


/* universal exporter XXX */
int fimg_export_picture(FloatImg *pic, char *fname, int flags);

/*	PNM files module		*/
int fimg_save_as_pnm(FloatImg *head, char *fname, int flags);
int fimg_load_from_pnm(char *fname, FloatImg *head, int notused);

double fimg_timer_set(int whot);
double fimg_timer_get(int whot);

/*	--> lib/contrast.c		*/
int fimg_id_contraste(char *name);
int fimg_square_root(FloatImg *s, FloatImg *d, double maxval);
int fimg_power_2(FloatImg *s, FloatImg *d, double maxval);
int fimg_cos_01(FloatImg *s, FloatImg *d, double maxval);
int fimg_cos_010(FloatImg *s, FloatImg *d, double maxval);
int fimg_mix_rgb_gray(FloatImg *img, float mix);

/*	funcs/saturation.c		*/
int     fimg_shift_to_zero(FloatImg *s, FloatImg *d, float coefs[6]);
int     fimg_auto_shift_to_zero(FloatImg *s, FloatImg *d);

/*	--> funcs/plasmas.c		*/
int fimg_prototype_plasma(FloatImg *img, double time, int type);

/* * * * experimental ! */
int fimg_classif_trial(FloatImg *src, FloatImg*dst, float fval, int notused);
int fimg_qsort_rgb_a(FloatImg *psrc, FloatImg *pdst, int notused);
int fimg_qsort_rgb_b(FloatImg *psrc, FloatImg *pdst, int notused);

/*	module funcs/??????.c		*/
int fimg_equalize_compute(FloatImg *src, void *vptr, float vmax);

int fimg_mk_gray_from(FloatImg *src, FloatImg*dst, int k);
int fimg_desaturate(FloatImg *src, FloatImg *dst, int notused);

/*	module funcs/geometry.c		*/
		/* warning, this module is a mess */
int fimg_halfsize_0(FloatImg *src, FloatImg *dst, int notused);
int fimg_halfsize_1(FloatImg *src, FloatImg *dst, int notused);
int fimg_extractor(FloatImg *in, FloatImg *out, FimgArea51 *rect);
int fimg_mirror(FloatImg *src, FloatImg *dst, int notused);


int fimg_incrustator_0(FloatImg *psrc, FloatImg *pdst,
				int xpos, int ypos, int flags);

int fimg_displacement_0(FloatImg *psrc, FloatImg *pdst, int flags);

/*	module funcs/rampes.c		*/
int fimg_hdeg_a(FloatImg *img, double dcoef);
int fimg_vdeg_a(FloatImg *img, double dcoef);

/*	FIMG files module		*/
int   fimg_fileinfos(char *fname, int *datas);
int   fimg_dump_to_file(FloatImg *head, char *fname, int notused);
int   fimg_load_from_dump(char *fname, FloatImg *where);
int   fimg_create_from_dump(char *fname, FloatImg *head);

int fimg_save_R_as_fits(FloatImg *src, char *outname, int flags);
int fimg_save_G_as_fits(FloatImg *src, char *outname, int flags);
int fimg_save_B_as_fits(FloatImg *src, char *outname, int flags);
int fimg_save_plane_as_fits(FloatImg *src, char *oname, char plane, int flags);

int fimg_write_as_tiff(FloatImg *src, char *fname, int flags);
int fimg_save_as_exr(FloatImg *src, char *outname, int flags);


/*	mathematics operations 				*/
float fimg_get_maxvalue(FloatImg *head);
int   fimg_get_minmax_rgb(FloatImg *head, float mmvals[6]);
int   fimg_meanvalues(FloatImg *head, float means[4]);
int   fimg_to_gray(FloatImg *head);
int   fimg_add_cste(FloatImg *fi, float value);
int   fimg_mul_cste(FloatImg *fi, float value);
int   fimg_ajust_from_grab(FloatImg *fi, double maxima, int notused);
void  fimg_drand48(FloatImg *fi, float kmul);
long   fimg_count_negativ(FloatImg *fi);
long   fimg_clamp_negativ(FloatImg *fi);

/*	various funcs modules		*/
int   fimg_load_from_png(char *filename, FloatImg *fimg);
int   fimg_create_from_png(char *filename, FloatImg *fimg);
int   fimg_save_as_png(FloatImg *src, char *outname, int flags);

int   fimg_save_as_bmp(FloatImg *src, char *outname, int flags);

int   fimg_test_pattern(FloatImg *fimg, int type, double dval);
int   fimg_draw_something(FloatImg *fimg);
int   fimg_mircol_1(FloatImg *dst, float mval);
int   fimg_multirandom(FloatImg *fimg, long nbpass);

/*	file is 'funcs/utils.c'		*/
void fimg_print_minmax(float minmax[6], char *titre);
int parse_WxH(char *str, int *pw, int *ph);
int parse_double(char *str, double *dptr);
int print_rectangle(char *str, FimgArea51 *rect);
int parse_rectangle(char *str, FimgArea51 *r, int notused);
int format_from_extension(char *fname);
char * extension_from_format(int fmt);




