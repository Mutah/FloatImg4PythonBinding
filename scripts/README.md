# Exemples de scripts

_Attention_, ce ne sont que des exemples, pas forcément adaptés
à une utilisation dans le monde réel.

## shoot.sh

Front-end de prise de photographies floues. C'est un script assez
simple à configurer : les valeurs par défaut fonctionnent.
Il faut juste renseigner l'emplacement de votre `grabviseq`
en début du script.

## contrast-test.sh

Démonstrateur d'ajustements de contraste. La configuration est
en dur dans le code.

## echomix.sh

Comment générer des videos psychotiques avec un peu de bash.
Ce script est expliqué dans la documentation PDF.



