#!/bin/bash


# change this to point to your installed binary
#
GVS=${HOME}/Devel/FloatImg/v4l2/grabvidseq

#	------------------------------------
#	set some default values	
DEV=/dev/video2
SZ=640x480
NBRE=320
PERIOD=0.0
COUNT=compteur
OPTIONS=" -v "
SHOW="no"
# output format can be of those types:
#      .pnm .fimg or .png
OFORMAT="P_%04d.pnm"

#		ces parametres peuvent etre surcharges avec
#		un fichier nomme "reglages" dans le repertoire
#		de travail.

#	------------------------------------
#	overide parameters from $PWD
if [ -r ./reglages ]
then
	source ./reglages
fi

#	------------------------------------
#	get the current picture number
if [ -r $COUNT ]
then
	numero=$( head -1 $COUNT )
else
	numero=1
fi

#	------------------------------------
#	make the output filename
if [ 1 -eq $# ]
then 
	outfile="$1"
else
	outfile=$( printf ${OFORMAT} $numero )
fi

#	------------------------------------
#	grab and display the fancy picture
$GVS -d $DEV -n $NBRE -p $PERIOD $OPTIONS -s $SZ -o $outfile

if [ ${SHOW} == "yes" ]
then
	display $outfile &
fi

#	------------------------------------
#	increment and save the picture number
numero=$(( numero + 1 ))
echo $numero > $COUNT


