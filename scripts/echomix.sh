#!/bin/bash

		###############################
		#           ECHOMIX           #
		###############################

SRCDIR="Fist"
DSTDIR="Pong"
FTMP="/dev/shm/tmp.fimg"
FDST="/dev/shm/foo.fimg"

#   ------------------------------------------------------------

# count the nomber of picz in the source directory
#
NBRE=$(ls -1 ${SRCDIR}/*.fimg | wc -l)

# compute the echo picz offset
#
OFFS=$(( NBRE / 4 ))

#   ------------------------------------------------------------
#		MAIN LOOP

for idx in $(seq 0 $NBRE)
do
	# build the two input filenames ...
	#
	imgA=$(printf "$SRCDIR/%04d.fimg" $idx)
	vb=$(( $(( idx + OFFS )) % NBRE))
	imgB=$(printf "$SRCDIR/%04d.fimg" $vb)

	#	... and the output filename
	#
	dst=$(printf "%s/%05d.png" ${DSTDIR} $idx)

	printf "  %20s   %20s   %8df   %20s\n" $imgA $imgB $vb $dst

	# trying to autocompute the mixing coefficient
	#
	compute=" s(${idx} / 16) "
	K=$(echo $compute | bc -l)
	printf " %25s  =>  %8.3f\n" "$compute" $K

	#	do the hard floating computation
	#
	fimgfx  -v cos010 ${imgB} ${FTMP}
	fimgops    -k ${K} ${FTMP} ${imgA} mix ${FDST}

	#	write the output as PNG for video encoding
	#
	fimg2png ${FDST} ${dst}

done

#   ------------------------------------------------------------
