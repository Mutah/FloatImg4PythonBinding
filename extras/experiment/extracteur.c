/*
 *		another ugly experiment
 */
#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>

#include  "../floatimg.h"

// #include  "incrustator.h"

int		verbosity;

/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */

/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */
int essai_extraction(FloatImg *in, FloatImg *out, FimgArea51 *rect)
{
int		foo;
int		xs, ys, xd, yd;
int		count;
float		rgb[3];

if (verbosity) {
	fimg_describe(in,  "source");
	fimg_describe(out, "destination");
	print_rectangle(__func__, rect);
	}

count = 0;
for (yd=0; yd<rect->h; yd++) {
	ys = yd + rect->y;
	if ((ys<0) || (ys>=in->height)) continue;
	for (xd=0; xd<rect->w; xd++) {
		xs = xd + rect->x;
		if ((xs<0) || (xs>=in->width)) continue;
		fimg_get_rgb(in,  xs, ys, rgb);
		fimg_put_rgb(out, xd, yd, rgb);
		count++;
		}
	}

// fprintf(stderr, "%s: %d pix moved\n", __func__, count);

return 0;
}
/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */
void help(int k)
{
puts("Usage:\n\textracteur in.fimg w,h,x,y out.???");
}
/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */

int main(int argc, char *argv[])
{
int		foo, opt;
FloatImg	src, dst;
FimgArea51	zone;
char		*infile  = "foo.fimg";
char		*outfile = "out.fimg";

verbosity = 0;

#if 0
for (foo=0; foo<argc; foo++) {
	fprintf(stderr, "%9d   %s\n", foo, argv[foo]);
	}
#endif

while ((opt = getopt(argc, argv, "hv")) != -1) {
	switch(opt) {
		case 'h':	help(0), exit(0);	break;
		case 'v':	verbosity++;		break;
		default:				break;
		}
	}

if (3 != argc-optind) {
	fprintf(stderr, "---- %s errcli c=%d %d ----\n", argv[0],
					argc, argc-optind);
	help(1);
	exit(1);
	}

if (verbosity) {
	fprintf(stderr, "*** %s   %s %s\n", argv[0], __DATE__, __TIME__);
	fimg_print_version(1);
	}

infile = argv[optind];	outfile = argv[optind+2];
fprintf(stderr, "%s   %s -> %s\n", argv[0], infile, outfile);

foo = fimg_create_from_dump(infile, &src);
if (foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
					foo, infile);
	exit(1);
	}

if (4 != parse_rectangle( argv[optind+1], &zone, 0) ) {
	fprintf(stderr, "%s: error in parsing of '%s'\n",
					argv[0], argv[optind+1]);
	exit(1);
	}

// zone.w = src.width / 2;		zone.h = src.height / 2;
// zone.x = src.width / 4 ;	zone.y = src.height / 4;

if (verbosity) print_rectangle(argv[0], &zone);

foo = fimg_create(&dst, zone.w, zone.h,  FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "NO PICZ EPIC FAIL %d\n", foo);
	exit(1);
	}

foo = fimg_extractor(&src, &dst, &zone);
if (foo) {
	fprintf(stderr, "EXTRACTOR EPIC FAIL %d\n", foo);
	exit(1);
	}

foo = fimg_export_picture(&dst, outfile, 0);
if (foo) {
	fprintf(stderr, "export '%s' -> err %d\n", outfile, foo);
	exit(1);
	}

return 0;
}
/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */
