/*
	testing some random funcs.

*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <math.h>
#include  <string.h>

#include  "floatimg.h"

int		verbosity;

/* --------------------------------------------------------------------- */
void fait_un_dessin(FloatImg *dessin)
{

fimg_draw_something(dessin);

}
/* --------------------------------------------------------------------- */
void help(int k)
{
puts("Options :");
puts("\t-d WxH\timage size");
exit(0);
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
FloatImg	fimgA, fimgB;
int		foo, opt;
int		W = 800, H = 600;
double		tb;

while ((opt = getopt(argc, argv, "d:hv")) != -1) {
	switch(opt) {
		case 'd':	parse_WxH(optarg, &W, &H);
				break;
		case 'h':	help(0);		break;
		case 'v':	verbosity++;		break;
		}
	}

if (verbosity) fimg_print_version(0);

fimg_create(&fimgA, W, H, 3);
fimg_create(&fimgB, W, H, 3);

fimg_clear(&fimgA);

fimg_drand48(&fimgB, 100.0);
foo = fimg_dump_to_file(&fimgB, "B.fimg", 0);

fimg_timer_set(0);
#define NBP 500
for (foo=0; foo<NBP; foo++) {
	if (verbosity) {
		printf("%5d / %5d\n", foo, NBP);
		}
	fait_un_dessin(&fimgB);
	fimg_add_3(&fimgA, &fimgB, &fimgA);
	// fimg_mul(&fimgA, &fimgB, &fimgA);
	}
tb = fimg_timer_get(0);
fprintf(stderr, "%s = %f seconds\n", __func__, tb);
foo = fimg_save_as_pnm(&fimgA, "drand48.pnm", 0);
foo = fimg_dump_to_file(&fimgA, "drand48.fimg", 0);

fimg_destroy(&fimgA);
fimg_destroy(&fimgB);


return 0;
}

/* --------------------------------------------------------------------- */

