/*
 *		filterstack.h
 */

typedef struct {
	int	numero;		/* id in crapulator */
	char	*name;
	int	ival; 
	float	fval;
	} FilterSlot;

#define NUMBER_OF_STACK		8
#define FILTER_BY_STACK		8

typedef struct {
	int		count;
	FilterSlot	slots[FILTER_BY_STACK];
	} FilterStack;

int filterstack_init(int numid, int notused);

int filterstack_add(int numid, int code, int ival, float fval);

int filterstack_list(int numid, const char *txt);	/* XXX */

int filterstack_run(int numid, FloatImg *target, int notused);

int load_stack_from_file(int numid, char *fname, int notused);

int parse_filter_chain(int numid, char *argument);

