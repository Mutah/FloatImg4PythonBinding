/*
		SINGLE
	experimental and/or testing code, do not use in
	serious production.
*/

/* -------------------------------------------------------------- */
/*
 *	next:
 *	dest:
 *	fxchain:
 *	outfmt: see floatimg.h for FILE_TYPE_XXX constants
 */
int single_init(int next, char *dest, int fxchain, int outfmt);

int single_push_picture(FloatImg *pimg);

int single_print_state(char *title, int k);

/* -------------------------------------------------------------- */

