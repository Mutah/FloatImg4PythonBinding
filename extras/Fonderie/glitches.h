/*
 * 		glitches.h
 */

int microglitch(FloatImg *pimg, int notused);

int do_something(FloatImg *pimg, int notused);
int plot_multidots(FloatImg *picture, int notused);

int kill_a_random_line(FloatImg *pvictime, float level, int bits);
int kill_a_few_lines(FloatImg *who, float fval, int number);
int random_blocks(FloatImg *picture, int percent);

int un_petit_flou_8x8(FloatImg *picture, int x, int y);
int un_moyen_flou_8x8(FloatImg *picture, int x, int y);
int poke_a_random_pixel(FloatImg *picz, float fval, int kaboo);

int vertical_singlitch(FloatImg *picz, int xpos, float fv, float omega,
							float phi);
int multilines_shift_0(FloatImg *picz, int step, int nombre);

/* this is a wtf file */
