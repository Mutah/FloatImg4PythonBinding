/*
 *		crapulator.h
 */

/*
 *	the main function				*/

int crapulator(FloatImg *image, int id_effect, float fparam);

/*
 *	naming system					*/
void list_crapulors(char *texte);
char	*crap_name_from_number(int num);
int	 crap_number_from_name(char *name);

/*
 *	this generated file contains the #define
 *	for symbolic name of effect ids.
 */
#include  "crapdef.h"

