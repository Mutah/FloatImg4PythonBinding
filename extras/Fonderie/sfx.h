/*
 *	sfx.h - special effects for fonderie & interpolator
 *	---------------------------------------------------
 */

int incrustation_vignette(FloatImg *src, FloatImg *dst, int k);


int trinitron(FloatImg *pimg, int notused);

int bouger_les_pixels(FloatImg *pimg, int kaboo);
int octotree_classif(FloatImg *pimg, float fk, int notused);

int mirror_split(FloatImg *pimg, int kaboo);
int upside_down(FloatImg *pimg);

int des_bords_sombres_a(FloatImg *pimg, int offset);
int des_bords_sombres_b(FloatImg *pimg, int offset);

int brotche_rand48_a(FloatImg *fimg, float ratio, float mval);
int brotche_rand48_b(FloatImg *fimg, float ratio, float mval);
int colors_brotcher(FloatImg *fimg, float fval);

/*
 * see also "crapulator.h" for some #define's
 */
