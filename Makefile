# CC_OPTS = -Wall -fpic -g -pg  -no-pie -DDEBUG_LEVEL=0
# LD_OPTS = libfloatimg.a -pg -lm

LIB_DIR = build/lib

OBJ_DIR = build/obj

STATIC_LIB = $(LIB_DIR)/libfloatimg-lib.a

DYN_LIB = $(LIB_DIR)/libfloatimg.so

VERBOSITY = 0

all: 	lib funcs tools v4l2

# shared: $(DYN_LIB)

# $(DYN_LIB): lib funcs

lib:
	$(MAKE) -C src/lib all

funcs:	lib
	$(MAKE) -C src/funcs
	# link lib & funcs for shared library
	echo "int verbosity = $(VERBOSITY);" >  $(OBJ_DIR)/verbosity.c
	gcc -c $(OBJ_DIR)/verbosity.c -o $(OBJ_DIR)/verbosity.o
	gcc -shared $(OBJ_DIR)/*.o -lnetpbm -lpnglite -lcfitsio -ltiff	-lz -lm -o $(DYN_LIB)


tools: lib
	mkdir -p build/bin
	$(MAKE) -C src/tools

v4l2:
	mkdir -p build/bin
	$(MAKE) -C src/v4l2

clean:
	$(MAKE) -C src/lib clean
	$(MAKE) -C src/funcs clean
	$(MAKE) -C src/tools clean
	$(MAKE) -C src/v4l2 clean
	rm -rf "./build"

install: all
	cp src/floatimg.h /usr/local/include
	cp build/lib/* /usr/local/lib
	cp build/bin/* /usr/local/bin


# all:	essai

# #---------------------------------------------------------------

# essai:		essai.c libfloatimg.a floatimg.h Makefile
# 	gcc $(COPT) $< $(LDOPT) -lpnglite -lz -o $@

# #---------------------------------------------------------------

# TOTAR = *.[ch] Makefile *.sh *.md 			\
# 	doc/the*.tex doc/mk*.sh	doc/*.txt		\
# 	funcs/*.[ch] funcs/Makefile			\
# 	tools/*.[ch] tools/*.sh tools/README.md tools/Makefile \
# 	v4l2/*.[ch] v4l2/Makefile			\
# 	scripts/*.sh scripts/README.md			\
# 	lib/*.[ch] lib/Makefile

# lines:          $(TOTAR)
# 	@wc $(TOTAR) | sort -n

# tarball:        $(TOTAR)
# 	date > tarball
# 	ls $(TOTAR) | sed 's/^/FloatImg\//'  > MANIFEST
# 	( cd .. ; tar zcvf floatimg.tar.gz `cat FloatImg/MANIFEST` )
# #---------------------------------------------------------------

