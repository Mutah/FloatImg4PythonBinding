# FloatImg4PythonBinding

Un fork de la galactique bibliothèque FloatImg de tTh, réaménagé pour permettre une utilisation depuis Python avec l'embryonnaire binding https://git.tetalab.org/Mutah/python-FloatImg/ (seulement sur la branche develop pour le moment)

Pas de révolution mais :

 - production d'une bibliothèque partagée
 - arborescence remanié : les meubles ont changés de place : les sources sont dans `src`, les expérimentations dans `extra` et les binaires sont produits dans `build`
 - build system : un Makefile racine avec des cibles générales : `all` pour tout builder, `lib` pour la bibliothèque de base, `funcs`


# Traitement des images en virgule flottante.

C'est d"abord un ensemble de fonctions pour traiter des images avec une énorme dynamique
sur les niveaux de pixels. C'est aussi quelques outils pour traiter ces images.
Et c'est enfin plusieurs embryons de logiciel destiné à faire des photos floues,
voire même des [films flous](Fonderie/).

![horloge floue](http://la.buvette.org/photos/cumul/horloge.png "horloge floue")


Il y a une [description](http://la.buvette.org/photos/cumul/) bien plus 
pas trop longue pour les curieux, et un début de
[documentation](http://la.buvette.org/photos/cumul/the_floatimg_hack.pdf)
pour les codeurs.
Le service après-vente est (plus ou moins bien) assuré sur
la [mailing list](https://lists.tetalab.org/mailman/listinfo/tetalab) et/ou
le canal IRC #tetalab sur le réseau de 
[Freenode](https://webchat.freenode.net/)...

Par ailleurs, d'autres expérimentations sont
[en cours](http://la.buvette.org/photos/cumul/fonderie/vidz.html#interpolator)
sur le traitement et l'assemblage de ces images floues dans le but de faire
des films flous. 

*En avant vers l'infini, et au-delà...*

## Dépendances

Bien entendu, avant tout, il faut installer quelques outils et
dépendances. Je vais tenter de les lister dans le plus grand
désordre (à la sauce Debian) :

```bash
sudo apt install libtiff-dev libpnglite-dev liblo-dev libv4l2-dev libcfitsio-dev libnetpbm-dev
```

Sauce Ubuntu :

```bash
sudo apt  install  libtiff-dev libpnglite-dev liblo-dev libv4l-dev libcfitsio-dev libnetpbm10-dev libncurses5-dev
```


Certains outils externes sont aussi utiles :

- gnuplot
- ImageMagick
- LaTeX

## Documentation

Encore trop légère, mais déja [présente](doc/).
C'est à vous de compiler le
[PDF](http://la.buvette.org/photos/cumul/the_floatimg_hack.pdf)

*Your mileage may vary...*
